package enabel.entities;

import java.util.Date;

public class RDV {

	private int id;
	private Patient patient;
	private Date date; 
	private int creneau;
	
	
	public RDV() {
		
	}
	public RDV(int id, Patient patient, Date date, int creneau) {
		
		this.id = id;
		this.patient = patient;
		this.date = date;
		this.creneau = creneau;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getCreneau() {
		return creneau;
	}
	public void setCreneau(int creneau) {
		this.creneau = creneau;
	}
	
}


