package enabel.beans;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.hibernate.Transaction;
import org.hibernate.Session;
import java.io.Serializable;
import enabel.entities.Patient;
import enabel.util.HibernateUtil;


@ManagedBean(name = "patientBean", eager = true)
@SessionScoped

public class PatientBean extends PersonneBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String SUCCESS = "success";
	private final String ERROR = "error";

	private String profession;
	private String sexe;
	private String age;
	private String poids;

	/**
	 * @param id
	 * @param nom
	 * @param prenom
	 * @param adresse
	 * @param telephone
	 * @param motdepasse
	 * @param profession
	 * @param sexe
	 * @param age
	 * @param poids
	 */
	public PatientBean(int id, String nom, String prenom, String adresse, String telephone, String motdepasse,
			String profession, String sexe, String age, String poids) {
		super(id, nom, prenom, adresse, telephone, motdepasse);
		this.profession = profession;
		this.sexe = sexe;
		this.age = age;
		this.poids = poids;
	}

	/**
	 * @param id
	 * @param nom
	 * @param prenom
	 * @param adresse
	 * @param telephone
	 * @param motdepasse
	 */
	public PatientBean(int id, String nom, String prenom, String adresse, String telephone, String motdepasse) {
		super(id, nom, prenom, adresse, telephone, motdepasse);
	}

	/**
	 * 
	 */
	public PatientBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String save() {
		
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		Transaction tx=null;
		
		// start transaction
		try {
			tx= session.beginTransaction();
			
			Patient patient = new Patient();
		
			
			patient.setNom(this.getNom());

			patient.setMotdepasse(this.getMotdepasse());

			session.save(patient);
			// end transaction
			session.getTransaction().commit();
			System.out.println("save in database");
			return SUCCESS;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ERROR;
		}
	}

}
