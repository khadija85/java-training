package enabel.beans;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.hibernate.Session;
import org.hibernate.Transaction;
 
@ManagedBean
@SessionScoped
 
public class CalendarBean {

	//@author Asouilda
	
	// for each bean we make jsf page 
	
	private int annee;
	private String mois;
	private String jour;
	
	
	public CalendarBean() {
		
	}
	public CalendarBean(int annee, String mois, String jour) {
	
		this.annee = annee;
		this.mois = mois;
		this.jour = jour;
	}
	public int getAnnee() {
		return annee;
	}
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	public String getMois() {
		return mois;
	}
	public void setMois(String mois) {
		this.mois = mois;
	}
	public String getJour() {
		return jour;
	}
	public void setJour(String jour) {
		this.jour = jour;
	}
	
}

	
	

