package enabel.beans;

import java.io.Serializable;


public class PersonneBean implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private int id;
		private String nom;
		private String prenom;
		private String adresse;
		private String telephone;
		private String motdepasse;
		/**
		 * @param id
		 * @param nom
		 * @param prenom
		 * @param adresse
		 * @param telephone
		 * @param motdepasse
		 */
		public PersonneBean(int id, String nom, String prenom, String adresse, String telephone, String motdepasse) {
			super();
			this.id = id;
			this.nom = nom;
			this.prenom = prenom;
			this.adresse = adresse;
			this.telephone = telephone;
			this.motdepasse = motdepasse;
		}
		/**
		 * 
		 */
		public PersonneBean() {
			super();
		}
		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}
		/**
		 * @param id the id to set
		 */
		public void setId(int id) {
			this.id = id;
		}
		/**
		 * @return the nom
		 */
		public String getNom() {
			return nom;
		}
		/**
		 * @param nom the nom to set
		 */
		public void setNom(String nom) {
			this.nom = nom;
		}
		/**
		 * @return the prenom
		 */
		public String getPrenom() {
			return prenom;
		}
		/**
		 * @param prenom the prenom to set
		 */
		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}
		/**
		 * @return the adresse
		 */
		public String getAdresse() {
			return adresse;
		}
		/**
		 * @param adresse the adresse to set
		 */
		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}
		/**
		 * @return the telephone
		 */
		public String getTelephone() {
			return telephone;
		}
		/**
		 * @param telephone the telephone to set
		 */
		public void setTelephone(String telephone) {
			this.telephone = telephone;
		}
		/**
		 * @return the motdepasse
		 */
		public String getMotdepasse() {
			return motdepasse;
		}
		/**
		 * @param motdepasse the motdepasse to set
		 */
		public void setMotdepasse(String motdepasse) {
			this.motdepasse = motdepasse;
		}
		
		

	}

	
	
	

