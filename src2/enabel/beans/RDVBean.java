package enabel.beans;

import java.util.Date;

public class RDVBean {

	private int id;
	private PatientBean patient;
	private Date date; 
	private int creneau;
	
	
	public RDVBean() {
		
	}
	public RDVBean(int id, PatientBean patient, Date date, int creneau) {
		
		this.id = id;
		this.patient = patient;
		this.date = date;
		this.creneau = creneau;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public PatientBean getPatient() {
		return patient;
	}
	public void setPatient(PatientBean patient) {
		this.patient = patient;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getCreneau() {
		return creneau;
	}
	public void setCreneau(int creneau) {
		this.creneau = creneau;
	}
	
}


